// --------------------------------------------------------------------

// using System.Collections.Generic;
// using UnityEngine;

// public class Particle
// {
//     private Vector2 acceleration;
//     private Vector2 velocity;
//     private Vector2 location;
//     private float lifespan;

//     public Vector2 Location => location;

//     public Particle(Vector2 l)
//     {
//         acceleration = Vector2.zero;
//         float vx = Random.Range(-0.3f, 0.3f);
//         float vy = Random.Range(-1.3f, -0.7f);
//         velocity = new Vector2(vx, vy);
//         location = l;
//         lifespan = 100.0f;
//     }

//     public void Run()
//     {
//         Update();
//     }

//     public void ApplyForce(Vector2 f)
//     {
//         acceleration += f;
//     }

//     private void Update()
//     {
//         velocity += acceleration;
//         location += velocity;
//         lifespan -= 2.5f;
//         acceleration = Vector2.zero; // Clear acceleration
//     }
//     public bool Dead()
//     {
//         return lifespan <= 0.0f;
//     }
// }

// public class ParticleSystemController2 : MonoBehaviour
// {
//     private List<Particle> particles;

//     public GameObject particlePrefab;
//     private Vector2 origin;

//     private void Start()
//     {
//         int numParticles = 100; // Adjust as needed

//         particles = new List<Particle>(numParticles);
//         // origin = new Vector2(Screen.width / 2, Screen.height / 2);
//         origin = new Vector2(-0.3f, 0.8f);
//         // Initialize particles here
//         for (int i = 0; i < numParticles; i++)
//         {
//             particles.Add(new Particle(origin));
//         }
//     }
//     private void Update()
//     {
//         Vector2 wind = new Vector2(0.35f, 0.06f); // Adjust wind strength as needed
//         ApplyForce(wind);
//         Run();
//         AddParticle();
//         Debug.Log(particles.Count);
//     }

//     private void Run()
//     {
//         particles.RemoveAll(p => p.Dead());
//         foreach (Particle particle in particles)
//         {
//             particle.Run();
//         }
//     }

//     private void ApplyForce(Vector2 dir)
//     {
//         foreach (Particle p in particles)
//         {
//             p.ApplyForce(dir);
//         }
//     }

//     private void AddParticle()
//     {
//         for (int i = 0; i < 2; i++)
//         {
//             particles.Add(new Particle(origin));
//         }

//     }

//     private void OnDrawGizmos()
//     {
//         Gizmos.color = Color.white;
//         if (particles != null)
//             foreach (Particle particle in particles)
//             {
//                 Gizmos.DrawSphere(particle.Location, 1f); // Adjust size as needed
//                 GameObject p = Instantiate(particlePrefab, particle.Location, Quaternion.identity);
//                 p.SetActive(true);
//             }
//     }
// }

// -----------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

public class Particle
{
    private Vector2 acceleration;
    private Vector2 velocity;
    private Vector2 location;
    private float lifespan;
    private GameObject gameObject;
    public Vector2 Location => location;

    public Particle(Vector2 l, GameObject particlePrefab)
    {
        acceleration = Vector2.zero;
        float vx = Random.Range(-0.3f, 0.3f);
        float vy = Random.Range(-1f, -0.7f);
        velocity = new Vector2(vx, vy);
        // float vx = Random.Range(-0.1f, 0.1f);
        // float vy = Random.Range(-1.5f, -0.5f);
        // velocity = new Vector2(vx, vy);
        location = l;
        gameObject = GameObject.Instantiate(particlePrefab, l, Quaternion.identity);
        gameObject.SetActive(true);

        // float baseLifespan = 100.0f;
        // lifespan = baseLifespan * Random.Range(0.8f, 1.2f);
        lifespan = 100.0f;
    }

    public void Run()
    {
        Update();
    }

    public void ApplyForce(Vector2 f)
    {
        acceleration += f;
    }

    private void Update()
    {
        velocity += acceleration;
        location += velocity;
        gameObject.transform.position = location; // Set position here
        lifespan -= 2.5f;
        acceleration = Vector2.zero;
        if (lifespan <= 0.0f)
        {
            gameObject.SetActive(false);
        }
    }

    public bool Dead()
    {
        return lifespan <= 0.0f;
    }
    public void destroyGameObject()
    {
        GameObject.Destroy(gameObject);
    }
}

public class ParticleSystemController2 : MonoBehaviour
{
    private List<Particle> particles;
    public GameObject particlePrefab;
    private Vector2 origin;

    private void Start()
    {
        int numParticles = 100; // Adjust as needed
        particles = new List<Particle>(numParticles);
        origin = new Vector2(-0.3f, 0.8f);
        for (int i = 0; i < numParticles; i++)
        {
            particles.Add(new Particle(origin, particlePrefab));
        }
    }

    private void Update()
    {
        Vector2 wind = new Vector2(0f, 0.6f); // Adjust wind strength as needed
        // Vector2 wind = GetRandomWind(0.35f, 0.2f);
        Vector2 mousePosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        float forceStrength = 0.2f; // Adjust force strength as needed
        float dx = Mathf.Lerp(-forceStrength, forceStrength, mousePosition.x);
        float dy = Mathf.Lerp(-forceStrength, forceStrength, mousePosition.y);
        Vector2 myWind = new Vector2(dx, dy); // Adjust wind strength as needed

        ApplyForce(myWind);
        Run();
        AddParticle();
        Debug.Log(particles.Count);
    }
    // public Vector2 GetRandomWind(float baseWindStrength, float windDirectionVariation)
    // {
    //     float windDirection = Random.Range(-windDirectionVariation, windDirectionVariation);
    //     float windStrength = baseWindStrength * Random.Range(0.8f, 1.2f);
    //     return new Vector2(Mathf.Cos(windDirection) * windStrength, Mathf.Sin(windDirection) * windStrength);
    // }

    private void Run()
    {
        for (int i = particles.Count - 1; i >= 0; i--)
        {
            Particle particle = particles[i];
            if (particle.Dead())
            {
                particle.destroyGameObject();
                particles.RemoveAt(i);
            }
            else
            {
                particle.Run();
            }
        }
    }

    private void ApplyForce(Vector2 dir)
    {
        // float dampingFactor = 0.9f; // Adjust damping (higher = less force over time)

        foreach (Particle p in particles)
        {
            p.ApplyForce(dir);
        }
    }

    private void AddParticle()
    {
        for (int i = 0; i < 3; i++)
        {
            particles.Add(new Particle(origin, particlePrefab));
        }
    }
}
